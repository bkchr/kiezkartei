<?php /* Template Name: Organization Settings Page Template */
get_header(); ?>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script>
	adjustTopMargin(".content-registration");
	registerDeleteButton("deleteOrganization");

</script>
<script>
var serverMapData;
function refreshCategories(orgid) {
	$('#checkboxes').empty();
	$('#checkboxes').html('<p>Wählen Sie hier aus, bei welchen Spenden-Kategorien Sie gefunden werden möchten.</p>');
	// for each top category
	for (var i = 0; i < serverMapData.categories.length; i++) {
		if (serverMapData.categories[i].parent === null) {
			var categorySetHtml = '';
			categorySetHtml += '<fieldset class="categorySet" >';
							categorySetHtml += '	<div class="labeledCheckbox topCategory">';
											categorySetHtml += '		<input type="checkbox" id="category_' + serverMapData.categories[i].id + '" name="category[]" value="' + serverMapData.categories[i].id + '" />';
											categorySetHtml += '		<label for="category_' + serverMapData.categories[i].id + '">' + serverMapData.categories[i].name + '</label>';
							categorySetHtml += '	</div>';
					var containerInserted = false;
					// for each sub category
					for (var j = 0; j < serverMapData.categories.length; j++) {
						if (serverMapData.categories[j].parent == serverMapData.categories[i].id) {
							if (containerInserted === false) {
								containerInserted = true;
								categorySetHtml += '<a class="subCategoryContainerUncollapse">genauer...</a>';
								categorySetHtml += '<div class="subCategoryContainer collapsed">';
									}
											categorySetHtml += '	<div class="labeledCheckbox subCategory">';
															categorySetHtml += '		<input type="checkbox" id="category_' + serverMapData.categories[j].id + '" name="category_[]" value="' + serverMapData.categories[j].id + '" />';
															categorySetHtml += '		<label for="category_' + serverMapData.categories[j].id + '">' + serverMapData.categories[j].name + '</label>';
											categorySetHtml += '	</div>';
								}
							}
							if (containerInserted === true) {
						categorySetHtml += '</div>';
					}
			categorySetHtml += '</fieldset>';
			$('#checkboxes').append(categorySetHtml);
		}
	}

	var organization_id = parseInt(orgid, 10);
	for (var i = 0; i < serverMapData.org_cat_mapping.length; i++) {
		if (serverMapData.org_cat_mapping[i].organizationid == organization_id) {
			$("#category_" + serverMapData.org_cat_mapping[i].categoryid).attr("checked", true);
		}
	}
	$('.subCategoryContainerUncollapse').each(function() {
		$(this).click(function(e) {
			e.stopPropagation();
			var container = $(this).next();
			if (container.hasClass("collapsed")) {
				$(this).text("weniger...");
				container.toggleClass("collapsed");
			}
			else {
				$(this).text("genauer...");
				container.toggleClass("collapsed");
			}
		});
	});
	$('.topCategory input').change(function() {
		var input = $(this);
		var subInputs = input.parent().parent().find('.subCategory input');
		if(input.is(":checked")) {
			subInputs.prop('checked', true);
		}
		else {
			subInputs.prop('checked', false);
		}
	});
	$('.subCategory input').change(function() {
		var input = $(this);
		var superInput = input.parent().parent().parent().find('.topCategory input');
		var neighbourInputs = input.parent().parent().find('.subCategory input');
		if(!input.is(":checked")) {
			superInput.prop('checked', false);
		}
		else if (!neighbourInputs.is(":not(:checked)")) {
			superInput.prop('checked', true);
		}
	});
}
$(document).ready(function() {
	var data = {
		action : 'kiezkartei_get_mapdata',
		nonce : kiezkartei_ajax.nonce
	};
	jQuery.post(kiezkartei_ajax.url, data, function(response) {
		if(response.success && response.data !== 0) {
			serverMapData = response.data;
			getUserdata(<?php echo(json_encode(get_current_user_id())); ?>);
			//refreshCategories(null);
		}
	});
});
</script>
<div id="registration-form-result-dialog" hidden>
</div>
<div id="reg-content" class="content-registration" role="main">
	<h1>Daten editieren</h1>
	<form id="registration-form" name="registration-form" class="pure-form pure-form-aligned">
		<fieldset>
			<div class="pure-control-group">
				<label><h3>Allgemeine Daten</h3></label>
			</div>
			<input id="userid" type="hidden" name="userid" value="<?php echo(json_encode(get_current_user_id())); ?>">
			
			<div class="pure-control-group">
				<label for="organisation-name">Name der Organisation</label>
				<input id="organisation-name" name="organisation-name" type="text" placeholder="" required>
			</div>
			<div class="pure-control-group">
				<label for="organisation-email">E-Mail Adresse</label>
				<input id="organisation-email" name="organisation-email" type="email" placeholder="" required>
			</div>
			<div class="pure-control-group">
				<label for="organisation-profile">Profiltext <br/> (Beschreibung Ihrer Organisation)</label>
				<textarea id="organisation-profile" name="organisation-profile" required cols="50" rows="10"></textarea>
			</div>
			<div>Ihr Passwort können sie <u><a href="http://kiezkartei.de/wp-admin/profile.php">hier</a></u> ändern.
		</div>
		<div class="pure-control-group">
			<div class="separator"></div>
		</div>
		<div class="pure-control-group">
			<label><h3>Kontaktdaten für Standort</h3></label>
		</div>
		<div class="pure-control-group">
			<label for="organization-select">Bitte wählen Sie den Standort den Sie bearbeiten wollen.</label>
		<select id="organization-select" style="display:none"></select>
	</div>
	<div id="organizationaddress" style="display:none">
		<div class="pure-control-group">
			<label for="organisation-contact-name">Kontaktperson</label>
			<input id="organisation-contact-name" name="organisation-contact-name" type="text" placeholder=""
			required>
		</div>
		<div class="pure-control-group">
			<label for="organisation-contact-email">Kontakt-E-Mail</label>
			<input id="organisation-contact-email" name="organisation-contact-email" type="email" placeholder=""
			required>
		</div>
		<div class="pure-control-group">
			<label for="organisation-contact-phone-number">Telefonnummer</label>
			<input id="organisation-contact-phone-number" name="organisation-contact-phone-number" type="text"
			placeholder="" required>
		</div>
		<div class="pure-control-group">
			<label for="organisation-contact-street">Straße</label>
			<input id="organisation-contact-street1" name="organisation-contact-street1" type="text"
			placeholder="" required>
		</div>
		<div class="pure-control-group">
			<label for="organisation-contact-city">Stadt</label>
			<input id="organisation-contact-city1" name="organisation-contact-city1" type="text" placeholder=""
			required>
		</div>
		<div class="pure-control-group">
			<label for="organisation-contact-zipcode">Postleitzahl</label>
			<input id="organisation-contact-zipcode1" name="organisation-contact-zipcode1" type="text"
			placeholder="" required>
		</div>
		<div class="pure-control-group">
			<label for="organisation-contact-url">Webseite</label>
			<input id="organisation-contact-url" name="organisation-contact-url" type="text" placeholder=""
			>
		</div>
		<div class="pure-control-group">
			<label for="organisation-current-need">Aktuelle Informationen oder dringend Benötigtes</label>
			<textarea id="organisation-current-need" name="organisation-current-need" class="organisation-current-need" type=""
			placeholder=""
			maxlength="200" cols="50" rows="10"></textarea>
		</div>

		<div>
			<div class="pure-control-group" id="checkboxes">
			</div>
		</div>
	</div>
	<input id="count-organizations" type="hidden" name="count" value="1">
	<input id="organizationid" type="hidden" name="orgid" value="">
	<div class="pure-controls">
		<button type="submit" class="pure-button pure-button-primary" id="editOrganization">Editieren</button>
		<button id="deleteOrganization" class="pure-button pure-button-primary" style="display:none;float:right">Standort Löschen</button>
		<button id="addOrganization" class="pure-button pure-button-primary" style="display:none">Hinzufügen</button>
	</div>
</fieldset>
</form>
</div><!-- #content -->
<?php get_footer(); ?>