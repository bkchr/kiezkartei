<?php get_header(); ?>


<script>
	adjustTopMargin(".content-news");
</script>

<div class="content-news" role="main">

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<a name="<?php the_ID(); ?>">
				<!-- Little trick with the style to get the browser to scroll to the right position -->
				<h1 style="padding-top: 40px; margin-top: -40px;"><?php the_title(); ?></h1>
			</a>
			<small><?php the_time('j F Y') ?></small>

			<div class="entry">
				<?php echo the_content(); ?>
			</div>
		</div>

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>
	
	<?php get_sidebar(); ?>

<div class="clearfix"></div>

</div>

<?php get_footer(); ?>
