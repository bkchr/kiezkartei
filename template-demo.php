<?php define( 'WP_USE_THEMES', false ); /* Template Name: Demo Page Template */ get_header(); ?>

<div id="content" role="main">

	<?php query_posts( 'posts_per_page=5' ); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			<small><?php the_time('F jS, Y') ?> <!-- by <?php the_author() ?> --></small>

			<div class="entry">
				<?php echo the_content(); ?>
			</div>

			<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments »', '1 Comment »', '% Comments »'); ?></p>
		</div>
	<?php endwhile; endif; ?>

</div><!-- #content -->


<?php get_footer(); ?>
