<?php get_header(); ?>

<script>
	adjustTopMargin(".content-page");
</script>

<div class="content-page" role="main">
	<?php
		the_post();
		$page_for_posts_id = get_option('page_for_posts');
		setup_postdata(get_page($page_for_posts_id));
		?>
		<div id="post-<?php the_ID(); ?>" class="page">
			<div id="editable-page-content">
				<?php the_content(); ?>
			</div>
		</div>
		<?php
		rewind_posts();
	?>
</div>

<?php get_footer(); ?>
