-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 26. Jan 2015 um 17:17
-- Server Version: 5.6.21
-- PHP-Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `test`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `requests`
--

CREATE TABLE IF NOT EXISTS `requests` (
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_german2_ci NOT NULL,
  `tel` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_german2_ci NOT NULL,
  `address` varchar(150) CHARACTER SET utf8 COLLATE utf8_german2_ci NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_german2_ci NOT NULL,
  `geolocation` varchar(32) CHARACTER SET utf8 COLLATE utf8_german2_ci NOT NULL,
  `id` int(10) NOT NULL,
  `category` varchar(32) CHARACTER SET utf8 COLLATE utf8_german2_ci NOT NULL,
  `text` varchar(256) CHARACTER SET utf8 COLLATE utf8_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `requests`
--

INSERT INTO `requests` (`name`, `tel`, `email`, `address`, `url`, `geolocation`, `id`, `category`, `text`) VALUES
('Bin Nackt', '', '', 'Wiener Straße 33\r\n10999 Berlin', '', '52.496669, 13.434098', 2, 'Clothes', 'Mir ist kalt.'),
('Habe Hunger', '030234565', 'hungry@sendpizza.de', 'Platz der Luftbrücke 43,\r\n12101 Berlin', '', '52.483238, 13.386102', 1, 'Food', 'Ich habe tierischen Hunger, ein Döner wäre geil!');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `requests`
--
ALTER TABLE `requests`
 ADD PRIMARY KEY (`category`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
