<?php /* Template Name: Register Page Template */
get_header(); ?>

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script>
	adjustTopMargin(".content-registration");
	registerRegistrationFormButton("kiezkartei_register_organization");
//	getCategoriesForRegistrationForm();
</script>

<script>

var serverRegisterData;

function refreshCategories() {
	$('#checkboxes').empty();
	$('#checkboxes').html('<p>Wählen Sie hier aus, bei welchen Spenden-Kategorien Sie gefunden werden möchten.</p>');

	// for each top category
	for (var i = 0; i < serverRegisterData.length; i++) {
		if (serverRegisterData[i][2] === null) {
			var categorySetHtml = '';
			categorySetHtml += '<fieldset class="categorySet" >';
			categorySetHtml += '	<div class="labeledCheckbox topCategory">';
			categorySetHtml += '		<input type="checkbox" id="category_' + serverRegisterData[i][0] + '_1" name="category1[]" value="' + serverRegisterData[i][0] + '" checked />';
			categorySetHtml += '		<label for="category_' + serverRegisterData[i][0] + '_1">' + serverRegisterData[i][1] + '</label>';
			categorySetHtml += '	</div>';
			var containerInserted = false;
			// for each sub category
			for (var j = 0; j < serverRegisterData.length; j++) {
				if (serverRegisterData[j][2] == serverRegisterData[i][0]) {
					if (containerInserted === false) {
						containerInserted = true;
						categorySetHtml += '<a class="subCategoryContainerUncollapse">genauer...</a>';
						categorySetHtml += '<div class="subCategoryContainer collapsed">';
					}
					categorySetHtml += '	<div class="labeledCheckbox subCategory">';
					categorySetHtml += '		<input type="checkbox" id="category_' + serverRegisterData[j][0] + '_1" name="category_1[]" value="' + serverRegisterData[j][0] + '" checked />';
					categorySetHtml += '		<label for="category_' + serverRegisterData[j][0] + '_1">' + serverRegisterData[j][1] + '</label>';
					categorySetHtml += '	</div>';
				}
			}
			if (containerInserted === true) {
				categorySetHtml += '</div>';
			}
			categorySetHtml += '</fieldset>';
			$('#checkboxes').append(categorySetHtml);
		}
	}

	$('.subCategoryContainerUncollapse').each(function() {
		$(this).click(function(e) {
			e.stopPropagation();
			var container = $(this).next();
			if (container.hasClass("collapsed")) {
				$(this).text("weniger...");
				container.toggleClass("collapsed");
			}
			else {
				$(this).text("genauer...");
				container.toggleClass("collapsed");
			}
		});
	});

	$('.topCategory input').change(function() {
		var input = $(this);
		var subInputs = input.parent().parent().find('.subCategory input');
		if(input.is(":checked")) {
			subInputs.prop('checked', true);
		}
		else {
			subInputs.prop('checked', false);
		}
	});

	$('.subCategory input').change(function() {
		var input = $(this);
		var superInput = input.parent().parent().parent().find('.topCategory input');
		var neighbourInputs = input.parent().parent().find('.subCategory input');
		if(!input.is(":checked")) {
			superInput.prop('checked', false);
		}
		else if (!neighbourInputs.is(":not(:checked)")) {
			superInput.prop('checked', true);
		}
	});
}

$(document).ready(function() {
	var data = {
		action : 'kiezkartei_get_categories',
		nonce : kiezkartei_ajax.nonce
	};

	jQuery.post(kiezkartei_ajax.url, data, function(response) {
		if(response.success && response.data !== 0) {
			serverRegisterData = response.data;
			refreshCategories();
		}
	});
});
</script>

<div id="registration-form-result-dialog" hidden>
</div>

<div id="reg-content" class="content-registration" role="main">
	<h1>Registrierung einer Organisation</h1>

	<p>Hier können Sie Ihre Organisation für die Kiezkartei registrieren. Sollte ihre Organisation mehrere Standorte haben, klicken Sie auf "Standort hinzufügen".</p>

	<form id="registration-form" name="registration-form" class="pure-form pure-form-aligned">
		<fieldset>
			<h3>Allgemeine Daten</h3>

			<div class="pure-control-group">
				<label for="organisation-name">Name der Organisation</label>
				<input id="organisation-name" name="organisation-name" type="text" placeholder="" required>
			</div>

			<div class="pure-control-group">
				<label for="organisation-email">E-Mail Adresse</label>
				<input id="organisation-email" name="organisation-email" type="email" placeholder="" required>
			</div>

			<div class="pure-control-group">
				<label for="organisation-password">Passwort</label>
				<input id="organisation-password" name="organisation-password" type="password" placeholder=""
					   oninput="checkPasswordEquality(document.getElementById('organisation-password-repeat'), this)"
					   required>
			</div>

			<div class="pure-control-group">
				<label for="organisation-password-repeat">Passwort wiederholen</label>
				<input id="organisation-password-repeat" type="password" placeholder=""
					   oninput="checkPasswordEquality(this, document.getElementById('organisation-password'))" required>
			</div>
			
			<div class="pure-control-group">
				<label for="organisation-profile">Profiltext <br /> (Beschreibung Ihrer Organisation)</label>
				<textarea id="organisation-profile" name="organisation-profile" required cols="50" rows="10"></textarea>
			</div>

			<div class="pure-control-group">
				<div class="separator"></div>
			</div>


			<div id="organizationaddress1">
				<h3>Kontaktdaten für Standort</h3>

				<div class="pure-control-group">
					<label for="organisation-contact-name">Kontaktperson</label>
					<input id="organisation-contact-name1" name="organisation-contact-name1" type="text" placeholder=""
						   required>
				</div>

				<div class="pure-control-group">
					<label for="organisation-contact-email">Kontakt-E-Mail</label>
					<input id="organisation-contact-email1" name="organisation-contact-email1" type="email" placeholder=""
						   required>
				</div>

				<div class="pure-control-group">
					<label for="organisation-contact-phone-number">Telefonnummer</label>
					<input id="organisation-contact-phone-number1" name="organisation-contact-phone-number1" type="text"
						   placeholder="" required>
				</div>

				<div class="pure-control-group">
					<label for="organisation-contact-street">Straße</label>
					<input id="organisation-contact-street1" name="organisation-contact-street1" type="text"
						   placeholder="" required>
				</div>

				<div class="pure-control-group">
					<label for="organisation-contact-city">Stadt</label>
					<input id="organisation-contact-city1" name="organisation-contact-city1" type="text" placeholder=""
						   required>
				</div>

				<div class="pure-control-group">
					<label for="organisation-contact-zipcode">Postleitzahl</label>
					<input id="organisation-contact-zipcode1" name="organisation-contact-zipcode1" type="text"
						   placeholder="" required>
				</div>

				<div class="pure-control-group">
					<label for="organisation-contact-url">Webseite</label>
					<input id="organisation-contact-url1" name="organisation-contact-url1" type="text" placeholder=""
						   >
				</div>
				<div class="pure-control-group">
					<label for="organisation-current-need">Aktuelle Informationen oder dringend Benötigtes</label>
					<textarea id="organisation-current-need1" name="organisation-current-need1" class="organisation-current-need"
							  maxlength="200" cols="50" rows="10"></textarea>
				</div>
				<div>
					<div id="checkboxes">
					</div>
				</div>


			</div>
							<a onclick="copyFormData()" title="Klicken um weitere Organisation hinzuzufügen." style="
				display: block;
				margin: 10px;
    cursor: pointer;
    font-size: 18px;
" >Standort hinzufügen +</a>
			<input id="count-organizations" type="hidden" name="count" value="1">
			<div class="pure-controls">
				<button type="submit" name="submitbutton" class="pure-button pure-button-primary">Registrieren</button>
			</div>
		</fieldset>
	</form>
</div><!-- #content -->


<?php get_footer(); ?>
