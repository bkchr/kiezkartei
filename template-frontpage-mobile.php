<?php /* Template Name: Front Page Mobile Template */
get_header(); ?>
<main role="main">
    <script>
        $(document).ready(function () {
            shortenArticleContent();
        });
    </script>

    <div id="fullpage">
        <div class="section">
            <!-- <div style="font-size: 6em; text-align: center;">Cooler Spruch!!<br> Kommt alle zu uns!! :D <br> Wir haben Cookies!</div>-->

            <div class="centerblock" style="text-align:center; margin-top: 5px;">
                <a href="<?php echo site_url(); ?>/map"><img alt="Logo" width="50%" src="<?php bloginfo('template_url') ?>/img/logofinal.png"></a>
                <br>
                <br><span style="font-size: 1.5em">Nicht verwendet,</span>
                <span style="font-size: 1.5em">gut gespendet.</span>
                <br>
                <p style="font-size: 1.5em; font-weight: bold;">Finde Hilfsorganisationen in der Nähe, um Menschen in deinem Kiez mit Spenden zu unterstützen.</p>
                <p style="font-size: 1.5em;">Damit Hilfe dort ankommt, <br> wo sie gebraucht wird.</p>
            </div>

        </div>
        <div class="section" style="margin-top: 40px;">
            <div style="height: 90%;">
                <div style="height: 90%; text-align: center;">
                    <p style="font-size: 1.5em;font-weight: bold;">Und so funktioniert's:</p>
                    <img width="100%" alt="Logo" src="<?php bloginfo('template_url') ?>/img/howtopicture.png">
                </div>
            </div>
        </div>

        <div class="section" style="margin-top: 40px;">
            <div style="height: 90%;">
                <div>
                    <div style="font-size: 1.5em; text-align:center">
                        <a href="<?php echo site_url() . '/karte'; ?>">
                        Suche jetzt nach Organisationen, die deine Spende benötigen &rarr;
                        </a>
                    </div>
                    <?php get_footer('main'); ?>
                </div>
            </div>
        </div>
    </div>
</main>