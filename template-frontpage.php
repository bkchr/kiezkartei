<?php /* Template Name: Front Page Template */
$page = 'home';
include('header.php'); ?>

    <main role="main">
        <script>
            $(document).ready(function () {
                $('#fullpage').fullpage({
                    anchors: ['firstPage', 'secondPage', 'thirdPage']
                });
                shortenArticleContent();
            });
        </script>

        <div id="fullpage">
            <div class="section">
                <div class="centerblock" style="text-align:center">
                    <a href="<?php echo site_url(); ?>/Karte"><img alt="Logo" src="<?php bloginfo('template_url') ?>/img/logofinal.png"></a>
                    <br>
                    <br>
                    <br><span style="font-size: 2em">Nicht verwendet,</span>
                    <span style="font-size: 2em">gut gespendet.</span>
                    <br>

                    <p style="font-size: 2.5em; font-weight: bold;">Finde Hilfsorganisationen in der Nähe, um Menschen in deinem Kiez mit Spenden zu unterstützen.</p>
                    <p style="font-size: 2em;">Damit Hilfe dort ankommt, wo sie gebraucht wird.</p>
                    <br>
                    <p style="font-size: 2.5em"><a href="#secondPage">&darr; Mehr Infos &darr;</a></p>
                </div>

            </div>

            <div class="section">
                <div style="height: 90%;">
                    <div style="height: 90%; text-align: center;">
                        <img width="900px" height="636px" alt="Logo" src="<?php bloginfo('template_url') ?>/img/howtopicture.png">
                        <br>
                        <p style="font-size: 2.5em"><a href="#thirdPage">&darr; Mehr Infos &darr;</a></p>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="centerblock" style="text-align:center">

                    <a href="<?php bloginfo('url') ?>/karte">
                        <div id="maphint">
                            <span>Hier geht es zur Karte!</span>
                        </div>
                        <img width="1000px" height="800px" alt="Logo" src="<?php bloginfo('template_url') ?>/img/map.png">
                    </a>
                </div>
            </div>
        </div>
    </main>
    </body>

    </html>