<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>
        <?php $offline = false; ?>
        <?php
			if($offline) {
				echo '<link rel="stylesheet" href="' . get_template_directory_uri() . '/offline/pure-min.css">';
			} else {
				echo '<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">';
			}
		?>
        <!--[if lte IE 8]>
            <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/grids-responsive-old-ie-min.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <?php
			if($offline) {
				echo '<link rel="stylesheet" href="' . get_template_directory_uri() . '/offline/grids-responsive-min.css">';
			} else {
				echo '<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/grids-responsive-min.css">';
			}
		?>
        <!--<![endif]-->

        <?php
			if($offline) {
				echo '<link rel="stylesheet" href="' . get_template_directory_uri() . '/offline/font-awesome.css">';
				echo '<script src="' . get_template_directory_uri() . '/offline/yui-min.js"></script>';
				echo '<script src="' . get_template_directory_uri() . '/offline/jquery.min.js"></script>';

				// leaflet
				echo '<link rel="stylesheet" href="' . get_template_directory_uri() . '/offline/leaflet.css" />';
				echo '<script src="' . get_template_directory_uri() . '/offline/leaflet.js"></script>';
			} else {
				echo '<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">';
				echo '<script src="http://yui.yahooapis.com/3.18.1/build/yui/yui-min.js"></script>';
				echo '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>';

				// leaflet
				echo '<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />';
				echo '<script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>';
			}
		?>

        <!--Fullpage.js-->
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.fullPage.css" />
        <script src="<?php echo get_template_directory_uri(); ?>/js/vendors/jquery.easings.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/vendors/jquery.slimscroll.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.fullPage.js"></script>

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

	</head>
	<?php  $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
	if(stripos($ua,'ndroid') !== false || stripos($ua,'iPhone') !== false) { ?>
	<body <?php body_class("mobile"); ?>>
	<?php } else {?>
	<body <?php body_class(); ?>>
	<?php }?>
		<!-- wrapper -->
		<div class="wrapper">
			<!-- header -->
			<header class="header clear" role="banner">

					<div class="header">

                        <div class="home-menu pure-menu pure-menu-open pure-menu-horizontal pure-menu-fixed pure-menu-kiezkartei">
                            <a class="pure-menu-heading" href="<?php echo site_url(); ?>"><img src="<?php echo site_url(); ?>/wp-content/themes/kiezkartei/img/logofinal_small.png" />Kiezkartei</a>
                            <?php if(is_user_logged_in()) { ?>
                                <form class="home-menu-form pure-form" action="<?php echo wp_logout_url(site_url()); ?>" method="post">
                                    <label><?php echo "Hallo " . wp_get_current_user()->display_name . "!"; ?></label>
                                    <button class="pure-button pure-button-primary">Ausloggen</button>
                                </form>
                            <?php }?>
                            <ul id="home-menu-items">
				<li><a href="<?php echo site_url(); ?>/"><?php echo (get_query_var('pagename')=='')?'<span>Home</span>':'Home'; ?></a></li>
                                <li><a href="<?php echo site_url(); ?>/blog"><?php echo (get_query_var('pagename')=='blog')?'<span>Blog</span>':'Blog'; ?></a></li>
                                <li><a href="<?php echo site_url(); ?>/werde-aktiv"><?php echo (get_query_var('pagename')=='werde-aktiv')?'<span>Werde aktiv</span>':'Werde aktiv'; ?></a></li>
                                <li><a href="<?php echo site_url(); ?>/karte"><?php echo (get_query_var('pagename')=='karte')?'<span>Karte</span>':'Karte'; ?></a></li>
                                <li><a href="<?php echo site_url(); ?>/ueber-uns"><?php echo (get_query_var('pagename')=='ueber-uns')?'<span>Über uns</span>':'Über uns'; ?></a></li>
                                <!--<li>
                                    <a href="#">Über uns</a>
                                    <ul>
                                        <li><a href="<?php echo site_url(); ?>/team">Team</a></li>
                                        <!--<li><a href="<?php echo site_url(); ?>/partner">Partner</a></li>
                                        <li><a href="<?php echo site_url(); ?>/faq">FAQ</a></li>-->
                                        <!--<li><a href="<?php echo site_url(); ?>/background">Hintergrund</a></li>
                                    </ul>
                                </li>-->
                                <?php if(is_user_logged_in()) { ?>
                                    <li><a href="<?php echo site_url(); ?>/settings"><?php echo (get_query_var('pagename')=='settings')?'<span>Angebote bearbeiten</span>':'Angebote bearbeiten'; ?></a></li>
                                <?php } else { ?>
                                    <li><a href="<?php echo site_url(); ?>/fuer-organisationen"><?php echo (get_query_var('pagename')=='fuer-organisationen')?'<span>Für Organisationen</span>':'Für Organisationen'; ?></a></li>
                                <?php } ?>
                            </ul>
						</div>

						<button id="mobileoptions">|||</button>
                    </div>

                    <script>
                        YUI({
                            classNamePrefix: 'pure'
                        }).use('gallery-sm-menu', function (Y) {

                            var horizontalMenu = new Y.Menu({
                                container         : '.home-menu',
                                sourceNode        : '#home-menu-items',
                                orientation       : 'horizontal',
                                hideOnOutsideClick: false,
                                hideOnClick       : false
                            });

                            horizontalMenu.render();
                            horizontalMenu.show();

                        });
						registerMobileButton();
					</script>
			</header>
			<!-- /header -->
