<?php /* Template Name: Karte */
get_header(); ?>

<script>
	adjustTopMargin("#map");
</script>
<script>
	// contains the data received from server
	var serverMapData = null;

	var orgCatMatching = null;

	var map = null;

	var currentMapLayer = null;

	function refreshCategories() {
		$('#mapFilter').empty();
		$('#mapFilter').html('<h1>Kategorien</h1><p>Wähle aus, was du spenden möchtest. Organisationen, die deine Spende entgegennehmen, siehst du dann weiter unten.</p>');

		// for each top category
		for (var i = 0; i < serverMapData.categories.length; i++) {
			if (serverMapData.categories[i].parent == null) {
				var categorySetHtml = '';
				categorySetHtml += '<fieldset class="categorySet" >';
				categorySetHtml += '	<div class="labeledCheckbox topCategory">';
				categorySetHtml += '		<input type="checkbox" id="category' + serverMapData.categories[i].id + '" name="category" value="' + serverMapData.categories[i].id + '" checked />';
				categorySetHtml += '		<label for="category' + serverMapData.categories[i].id + '">' + serverMapData.categories[i].name + '</label>';
				categorySetHtml += '	</div>';
				var containerInserted = false;
				// for each sub category
				for (var j = 0; j < serverMapData.categories.length; j++) {
					if (serverMapData.categories[j].parent == serverMapData.categories[i].id) {
						if (containerInserted == false) {
							containerInserted = true;
							categorySetHtml += '<a class="subCategoryContainerUncollapse">genauer...</a>';
							categorySetHtml += '<div class="subCategoryContainer collapsed">';
						}
						categorySetHtml += '	<div class="labeledCheckbox subCategory">';
						categorySetHtml += '		<input type="checkbox" id="category' + serverMapData.categories[j].id + '" name="category" value="' + serverMapData.categories[j].id + '" checked />';
						categorySetHtml += '		<label for="category' + serverMapData.categories[j].id + '">' + serverMapData.categories[j].name + '</label>';
						categorySetHtml += '	</div>';
					}
				}
				if (containerInserted == true) {
					categorySetHtml += '</div>';
				}
				categorySetHtml += '</fieldset>';
				$('div#mapFilter').append(categorySetHtml);
			}
		}

		$('.subCategoryContainerUncollapse').each(function() {
			$(this).click(function(e) {
				e.stopPropagation();
				var container = $(this).next();
				if (container.hasClass("collapsed")) {
					$(this).text("weniger...");
					container.toggleClass("collapsed");
				}
				else {
					$(this).text("genauer...");
					container.toggleClass("collapsed");
				}
			});
		});

		$('.topCategory input[name=category]').change(function() {
			var input = $(this);
			var subInputs = input.parent().parent().find('.subCategory input');
			if(input.is(":checked")) {
				subInputs.prop('checked', true);
			}
			else {
				subInputs.prop('checked', false);
			}
			refreshResults();
		});

		$('.subCategory input[name=category]').change(function() {
			var input = $(this);
			var superInput = input.parent().parent().parent().find('.topCategory input');
			var neighbourInputs = input.parent().parent().find('.subCategory input');
			if(!input.is(":checked")) {
				superInput.prop('checked', false);
			}
			else if (!neighbourInputs.is(":not(:checked)")) {
				superInput.prop('checked', true);
			}
			refreshResults();
		});
	}

	function generateOrgCatMatching() {
		var orgCatMatching = []

		for (var i = 0; i < serverMapData.organizations.length; i++) {
			var org = serverMapData.organizations[i];
			var currentOrgCatMatching = {
				'matchingCats': [],
				'org': org
			}

			// foreach selected cat search if org has cat
			var cats = $('#map input[name=category]:checked')
				.map(function() {
					return $(this).val();
				});
			for (var j = 0; j < serverMapData.org_cat_mapping.length; j++) {
				var catId = serverMapData.org_cat_mapping[j].categoryid;
				var orgId = serverMapData.org_cat_mapping[j].organizationid;
				if ($.inArray(catId, cats) != -1 && orgId == org.id) {
					currentOrgCatMatching.matchingCats.push(catId);
				}
			}

			// only add non-zero matching results
			if (currentOrgCatMatching.matchingCats.length > 0) {
				orgCatMatching.push(currentOrgCatMatching);
			}
		}

		orgCatMatching.sort(function (a,b) {
			return b.matchingCats.length - a.matchingCats.length
		});

		return orgCatMatching;
	}

	function refreshTextualResults() {
		$('#mapFilterResultsTextual').empty();

		if (orgCatMatching.length == 0) {
			$('#mapFilterResultsTextual').html('<h1>Keine Ergebnisse</h1>');
		}
		else {
			$('#mapFilterResultsTextual').html('<h1>' + orgCatMatching.length + ' Ergebnisse</h1><div class="results"></div>');
		}

		for (var i = 0; i < orgCatMatching.length; i++) {
			var orgCatMatchingHtml = '<div class="resultEntry" id="resultEntry' + i + '">';
			orgCatMatchingHtml += '<span class="orgrank">' + (i+1) + '.</span>';
			orgCatMatchingHtml += '<span class="orgname">' + orgCatMatching[i].org.name + '</span>';
			
			if (orgCatMatching[i].org.street.length > 0) {
				orgCatMatchingHtml += '<span class="orgstreet">' + normalizeString(orgCatMatching[i].org.street) + '</span>';
			}
			if (orgCatMatching[i].org.zipcode.length > 0 && orgCatMatching[i].org.city.length > 0) {
				orgCatMatchingHtml += '<span class="orgcity">' + orgCatMatching[i].org.zipcode + ' ' + normalizeString(orgCatMatching[i].org.city) + '</span>';
			}
			if (orgCatMatching[i].org.url.length > 0) {
				orgCatMatchingHtml += '<a class="orgwebsite" href="' + normalizeUrl(orgCatMatching[i].org.url) + '" target="_blank">Webseite besuchen</a>';
			}
			orgCatMatchingHtml += '<a class="showprofile">Profil anzeigen für weitere Informationen...</a>';
			orgCatMatchingHtml += '</div>';
			$('#mapFilterResultsTextual .results').append(orgCatMatchingHtml);
		}

		var callback = function() {
			$('#mapFilterResultsTextual .results').height($(window).height() - getHeaderHeight());
		};
		$(window).resize(callback);
		callback();
	}

	function initMap() {
		var callback = function() {
			$('#OpenStreetMapView').height($(window).height() - getHeaderHeight());
		};
		$(window).resize(callback);
		callback();

		// set up the map
		map = new L.Map('OpenStreetMapView', {
			zoomControl : false
		});

		// create the tile layer with correct attribution
		var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
		var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
		var osm = new L.TileLayer(osmUrl, {
			minZoom : 11,
			maxZoom : 20,
			attribution : osmAttrib
		});

		// start the map in Berlin
		map.setView(new L.LatLng(52.518173, 13.400790), 12);
		map.addLayer(osm);

		L.control.zoom({
			position : 'bottomleft'
		}).addTo(map);
	}

	function refreshMapView() {
		if (currentMapLayer != null) {
			map.removeLayer(currentMapLayer);
		}
		var icon = L.icon({
			iconUrl : "http://" + document.domain + "/wp-content/themes/kiezkartei/img/kiezkartei-pin.png",
			iconSize : [32, 37], // size of the icon
			iconAnchor : [16, 36], // point of the icon which will correspond to marker's location
			popupAnchor : [0, -18] // point from which the popup should open relative to the iconAnchor
		});

		var organizationMarker = [];

		for(var i = 0; i < orgCatMatching.length; i++) {
			var orgCatMatchingHtml = '<div class="resultEntry" id="resultEntry' + i + '">';
			orgCatMatchingHtml += '<span class="orgname">' + orgCatMatching[i].org.name + '</span>';
			
			if (orgCatMatching[i].org.street.length > 0) {
				orgCatMatchingHtml += '<span class="orgstreet">' + normalizeString(orgCatMatching[i].org.street) + '</span>';
			}
			if (orgCatMatching[i].org.zipcode.length > 0 && orgCatMatching[i].org.city.length > 0) {
				orgCatMatchingHtml += '<span class="orgcity">' + orgCatMatching[i].org.zipcode + ' ' + normalizeString(orgCatMatching[i].org.city) + '</span>';
			}
			if (orgCatMatching[i].org.url.length > 0) {
				orgCatMatchingHtml += '<a class="orgwebsite" href="' + normalizeUrl(orgCatMatching[i].org.url) + '" target="_blank">Webseite besuchen</a>';
			}
			orgCatMatchingHtml += '<a class="showprofile">Profil anzeigen</a>';
			orgCatMatchingHtml += '</div>';

			organizationMarker.push(L.marker([orgCatMatching[i].org.latitude, orgCatMatching[i].org.longitude], {icon : icon}).bindPopup(orgCatMatchingHtml));
		}
		currentMapLayer = L.layerGroup(organizationMarker);
		currentMapLayer.addTo(map);
	}

	function refreshResults() {
		orgCatMatching = generateOrgCatMatching();
		refreshTextualResults();
		refreshMapView();
	}

	function refreshAll() {
		refreshCategories();
		refreshResults();
	}

	function showProfile(id) {
		if (orgCatMatching.length > id) {
			$('#profile').css("display", "block");
			$('#mapFilter').css("display", "none");
			$('#mapFilterResultsTextual').css("display", "none");
			$('#mapView').css("display", "none");

			var org = orgCatMatching[id].org;
			var cats = [];
			for (var i = 0; i < serverMapData.org_cat_mapping.length; i++) {
				if (serverMapData.org_cat_mapping[i].organizationid == org.id) {
					cats.push(serverMapData.org_cat_mapping[i].categoryid);
				}
			}

			var html = '<a class="profileback"><span class="arrow-left"></span> Zurück zur Übersicht</a>';
			html += '<img alt="Logo" src="http://' + document.domain + '/wp-content/themes/kiezkartei/img/logofinal.png" width="150">';
			html += '<h1>' + org.name + '</h1>';
			html += '<h2>Beschreibung:</h2>';
			if (org.profile != null && org.profile.trim().length > 0) {
				html += '<div>' + org.profile + '</div>';
			}
			else {
				html += '<div>Keine Angabe.</div>';
			}
			html += '<h2>Kontaktdaten:</h2>';
			if (org.contact_name != null && org.contact_name.trim().length > 0) {
				html += '<div>Ansprechperson: ' + org.contact_name + '</div>';
			}
			if (org.contact_phone_number != null && org.contact_phone_number.trim().length > 0) {
				html += '<div>Telefon: ' + org.contact_phone_number + '</div>';
			}
			if (org.contact_email != null && org.contact_email.trim().length > 0) {
				html += '<div>E-Mail: ' + org.contact_email + '</div>';
			}
			html += '<div>Adresse: ';
			if (org.street.length > 0) {
				html += '<div>' + normalizeString(org.street) + '</div>';
			}
			if (org.zipcode.length > 0 && org.city.length > 0) {
				html += '<div>' + org.zipcode + ' ' + normalizeString(org.city) + '</div>';
			}
			if (org.url.length > 0) {
				html += '<div>Website: <a href="' + normalizeUrl(org.url) + '" target="_blank">' + org.url + '</a></div>';
			}
			html += '</div>';

			html += '<h2>Aktuelles und/oder dringend Benötigtes:</h2>';
			if (org.current_need != null && org.current_need.trim().length > 0) {
				html += '<div>' + org.current_need + '</div>';
			}
			else {
				html += '<div>Keine Angabe.</div>';
			}

			html += '<h2>Folgende Spenden werden entgegengenommen:</h2>';
			html += '<div>';
			// for each top category
			for (var i = 0; i < serverMapData.categories.length; i++) {
				if (serverMapData.categories[i].parent == null) {
					var hasSubCategory = false;
					// for each sub category
					for (var j = 0; j < serverMapData.categories.length; j++) {
						if (serverMapData.categories[j].parent == serverMapData.categories[i].id && $.inArray(serverMapData.categories[j].id, cats) != -1) {
							hasSubCategory = true;
						}
					}
					if ($.inArray(serverMapData.categories[i].id, cats) != -1 || hasSubCategory) {
						html += serverMapData.categories[i].name;
						html += '</br>';
						// for each sub category
						for (var j = 0; j < serverMapData.categories.length; j++) {
							if (serverMapData.categories[j].parent == serverMapData.categories[i].id && $.inArray(serverMapData.categories[j].id, cats) != -1) {
								html += '&nbsp;&nbsp;&nbsp;' + serverMapData.categories[j].name;
								html += '</br>';
							}
						}
					}
				}
			}
			html += '</div>';
			$('#profile').append(html);
		}
	}

	function hideProfile() {
		$('#profile').empty();
		$('#profile').css("display", "none");
		$('#mapFilter').css("display", "block");
		$('#mapFilterResultsTextual').css("display", "block");
		$('#mapView').css("display", "block");
	}

	function normalizeUrl(str) {
		if (str.indexOf('http://') === 0) {
			return str;
		}
		else {
			return 'http://' + str;
		}
	}

	function normalizeString(str) {
		return str.charAt(0).toUpperCase() + str.slice(1);
	}

	// start
	$(document).ready(function() {
		initMap();

		var data = {
			action : 'kiezkartei_get_mapdata',
			nonce : kiezkartei_ajax.nonce
			// anyother code etc
		};

		jQuery.post(kiezkartei_ajax.url, data, function(response) {
			if(response.success && response.data != 0) {
				serverMapData = response.data;
				refreshAll();
			}
		});

		// add click event to showprofile links
		$('#map').on('click', 'a.showprofile', function () {
			var id = $(this).parent().attr('id').substr(11);
			showProfile(id);
		});

		// add click event to showprofile links
		$('#map').on('click', 'a.profileback', function () {
			hideProfile();
		});

		// refresh map on toggle
		$('#toggleSize').click(function () {
			if ($('#mapFilterResultsTextual').hasClass("moreMap")) {
				$('#mapFilterResultsTextual').css("display", "block");
				$(this).text("vergrößern");
			}
			else {
				$('#mapFilterResultsTextual').css("display", "none");
				$(this).text("verkleinern");
			}
			$('#mapFilterResultsTextual').toggleClass("moreMap");
			$('#mapView').toggleClass("moreMap");

			$('#OpenStreetMapView').replaceWith('<div id="OpenStreetMapView" />');
			initMap();
			refreshMapView();
		});
	});
</script>

<div id="map">
	<div id="profile" style="display: none;">
		
	</div>
	<div id="mapFilter">
		Kategorien werden geladen...
	</div>

	<div id="mapFilterResultsTextual">
		Ergebnisse werden geladen...
	</div>

	<div id="mapView">
		<h1>Karte</h1> <a id="toggleSize">vergrößern</a>
		<div id="OpenStreetMapView">
			Karte wird geladen...
		</div>
	</div>
</div>
<!-- /wrapper -->
</body>
</html>

