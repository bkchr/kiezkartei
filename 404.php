<?php get_header(); ?>

	<script>
	  adjustTopMargin(".error-page");
	</script>

	<main class="error-page" role="main" style="text-align: center;">
		<article id="post-404">
		  <h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
		    <h2>
			<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
		    </h2>
		</article>
	</main>

<?php get_footer(); ?>
