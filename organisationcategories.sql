-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 02, 2015 at 10:23
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `organisationcategories` (
`categoryid` int(11) NOT NULL,
  `organizationid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `organisationcategories`
ADD CONSTRAINT `categories_ibfk_1`
FOREIGN KEY (`organizationid`) REFERENCES `organizations` (`id`)
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `organisationcategories`
ADD CONSTRAINT `categories_ibfk_2`
FOREIGN KEY (`categoryid`) REFERENCES `categories` (`id`)
ON UPDATE CASCADE
ON DELETE CASCADE;