--
-- Tabellenstruktur für Tabelle `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `name` varchar(32) COLLATE utf8_german2_ci NOT NULL,
  `tel` varchar(32) COLLATE utf8_german2_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_german2_ci NOT NULL,
  `address` varchar(150) COLLATE utf8_german2_ci NOT NULL,
  `geolocation` varchar(32) COLLATE utf8_german2_ci NOT NULL,
  `id` int(10) NOT NULL,
  `category` varchar(32) COLLATE utf8_german2_ci NOT NULL,
  `text` varchar(256) COLLATE utf8_german2_ci NOT NULL,
  `url` varchar(64) COLLATE utf8_german2_ci NOT NULL,
  PRIMARY KEY (`id`)
);

--
-- Daten für Tabelle `offers`
--

INSERT INTO `offers` (`name`, `tel`, `email`, `address`, `geolocation`, `id`, `category`, `text`, `url`) VALUES
('Steven Winter', '0163denresthabeichvergessen', 'steven.winter@mailbox.tu-berlin.de', 'Libauer Straße 15,\r\n10245 Berlin', '52.5088945,13.453696', 1, 'Clothes', 'Ich biete eine schöne Jacke.', ''),
('Jack Bauer', '0301289938', 'jack.bauer@24.com', 'Boxhagener Straße 49,\r\n10245 Berlin', '52.508324, 13.466370', 2, 'Food', 'Ich biete einen Döner.', '');
