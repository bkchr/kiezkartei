<?php /* Template Name: News Page Template */
$page = 'news';
include('header.php'); ?>

<script>
	adjustTopMargin(".content-news");
</script>



<div class="content-news" role="main">
	<?php get_sidebar(); ?>
	<div class="content-wrap">
		<div class="content-news-header" style="font-weight: bold; text-align: center;">News</div>
		<?php
			$posts_per_page = 5;
			$current_page = intval(get_query_var('page', 0));

			if($current_page < 0)
				$current_page = 0;

			if(($post_id = get_query_var('post', -1)) > -1) {
				$all_post_ids = get_posts(array('numberposts' => -1, 'fields' => 'ids'));

				for($i = 0; $i < count($all_post_ids); ++$i) {
					if($all_post_ids[$i] == $post_id) {
						$current_page = intval($i / $posts_per_page);
						break;
					}
				}
			}

			$published_posts = wp_count_posts()->publish;
			$number_of_pages = intval($published_posts / $posts_per_page);

			if($published_posts % $posts_per_page != 0)
				++$number_of_pages;

			$query_options = 'posts_per_page=' . $posts_per_page;

			if($current_page != 0 && $current_page < $number_of_pages)
				$query_options = $query_options . "&offset=" . ($current_page * $posts_per_page);



		 	query_posts($query_options);

			$post_count = 0;

			if(have_posts()) :
				while(have_posts()) :
					the_post(); ?>
					<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
						<a name="<?php the_ID(); ?>">
							<!-- Little trick with the style to get the browser to scroll to the right position -->
							<h1 style="padding-top: 40px; margin-top: -40px;"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
						</a>
						<small><?php the_time('j F Y') ?></small>
						<?php get_the_category( $post_id ); ?>

						<div class="entry">
							<?php the_content(); ?>
						</div>
					</div>

					<?php
						$post_count++;
						if($post_count < $posts_per_page && $post_count < ($published_posts - $current_page * $posts_per_page))
						{
							echo '<div class="separator"></div>';
						}
					?>
			<?php endwhile; endif; ?>
		<?php if($number_of_pages > 1) { ?>
			<div class="separator"></div>
			<div class="content-news-site-switch">
				<?php
					if(($current_page + 1) * $posts_per_page <= $published_posts) {
				?>
						<a href="<?php echo site_url() . '/news/?page=' . ($current_page + 1); ?>">Ältere News</a>
						<span style="padding-left: 30px"></span>
				<?php } ?>
				<?php
					if($current_page > 0) { ?>
						<a href="<?php echo site_url() . '/news/?page=' . ($current_page - 1); ?>">Neuere News</a>
				<?php } ?>
			</div>
		<?php } ?>

	</div><!-- .content-wrap -->

<div class="clearfix"></div>

</div><!-- #content -->


<?php get_footer(); ?>
