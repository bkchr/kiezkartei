/* ---------------------------------------------------------------- */
/* ---------------------- MARGIN TOP HANDLING --------------------- */
/* ---------------------------------------------------------------- */

function getHeaderHeight() {
	return $('.home-menu').height();
}

function adjustTopMargin(element) {
	$(document).ready(function() {
		var callback = function() {			
			$(element).css("margin-top", getHeaderHeight());
			$(element).css("padding", "5px");
		};
		$(window).resize(callback);
		callback();
		setTimeout(callback, 2000);
	});
}

function findLastPunctuationMarkPosition(mark, str) {
	var last_pos = str.length;
	while(true) {
		last_pos = str.lastIndexOf(mark, last_pos - 1);

		if(last_pos == -1)
			return -1;

		if(last_pos + 1 == str.length)
			return last_pos;

		// we want to be sure that the found punctuation mark, is a real punctuation mark
		// not any html code or maybe something more weird stuff
		// we search for this characters, if the appear directly behind our punctuation mark
		// that should hopefully filter out any other stuff
		var next_chars = [" ", "\r", "\r\n", "\n", "&nbsp;"];
		var i = 0;

		for(i = 0; i < next_chars.length; ++i) {
			if(str.indexOf(next_chars[i], last_pos) == last_pos + 1) {
				return last_pos;
			}
		}
	}
}

function getLastPunctuationMarkPosition(str) {
	var dot_pos = findLastPunctuationMarkPosition(".", str);
	var question_pos = findLastPunctuationMarkPosition("?", str);
	var exclamation_pos = findLastPunctuationMarkPosition("!", str);

	if(dot_pos == question_pos == exclamation_pos == -1)
		return str.length() - 1;

	return Math.max(dot_pos, question_pos, exclamation_pos);
}

function getBaseUrl() {
	var re = new RegExp(/^.*\//);
	return re.exec(window.location.href);
}

function shortenArticleContent() {
	var header_height = getHeaderHeight();
	var window_height = $(window).height();
	// we want to use 80% percent of the visible area for the title + the content of the article
	var usable_height = (window_height - header_height) * 0.8;

	$("article").each(function() {
		var title_height = $(this).children(".slider-article-title").height();
		var content = $(this).children(".slider-article-content");

		var remaining_height = usable_height - title_height;

		if(remaining_height >= content.height())
			return;

		var article_id = $(this).children(".slider-article-id").html();
		var site_url = getBaseUrl();
		var more_text = '<a href="' + site_url + '/news/?post=' + article_id + '#' + article_id + '" style="color: red;">mehr...</a>';

		var original_text = content.html();
		// we try to use the remaining_height to content.height() ratio to reduce the text in the first step
		var shorten_ratio = remaining_height / content.height();

		while(remaining_height < content.height()) {
			var shortened_text = original_text.substr(0, original_text.length * shorten_ratio);
			var last_punctuation_mark_pos = getLastPunctuationMarkPosition(shortened_text);

			var new_text = shortened_text.substr(0, last_punctuation_mark_pos + 1) + " " + more_text;
			content.html(new_text);

			// update the ratio to take a much smaller part
			shorten_ratio = shorten_ratio - 0.05;
		}
	});
}

function checkPasswordEquality(passwordrepeat_input, password_input) {
	if(passwordrepeat_input.value != password_input.value) {
		passwordrepeat_input.setCustomValidity('Die Passwörter stimmen nicht überein!');
	} else {
		// input is valid -- reset the error message
		passwordrepeat_input.setCustomValidity('');
	}
}

function showDialog(title, msg, okbuttonfunc, abbr) {
	$("#registration-form-result-dialog").removeAttr('hidden');
	$("#registration-form-result-dialog").html(msg);
	var myButtons = {
		OK : function() {
			$(this).dialog("close");
			if(okbuttonfunc) {
				okbuttonfunc();
			}
		}
	}
	if(abbr) {
		myButtons["Abbrechen"] = function() {
			$(this).dialog("close");
		}
	}
	$("#registration-form-result-dialog").dialog({
		modal : true,
		title : title,
		buttons : myButtons
	});

}

function getGeoposition(street, city, zipcode, i, callback) {
	$.getJSON('http://nominatim.openstreetmap.org/search?format=json&limit=1&q=' + encodeURIComponent(street + ',' + city + ',' + zipcode),
		function(data) {
			if(data.length == 1) {
				callback(data[0].lon, data[0].lat, i);
			} else {
				showDialog("Fehler in der Kontaktadresse " + i + "!", "Bitte überprüfen Sie die Kontaktadresse auf etwaige Fehler!", undefined);
			}
		});
}

function registerRegistrationFormButton(action) {
	$(document).ready(function() {
		$("#registration-form").unbind("submit");
		$("#registration-form").on('submit', function(event) {

			var request = $("#registration-form").serialize();
			request += "&action=" + action;
			request += "&nonce=" + kiezkartei_ajax.nonce;

			var organizationaddresses = $('#count-organizations').val();
			for(var i = 1; i <= organizationaddresses; i++) {
				getGeoposition($("#organisation-contact-street" + i).val(), $("#organisation-contact-city" + i).val(),
					$("#organisation-contact-zipcode" + i).val(), i, function(lon, lat, count) {
						request += "&lon" + count + "=" + lon;
						request += "&lat" + count + "=" + lat;
                                                console.log(lon);
						if(count == organizationaddresses) {
							if(organizationaddresses > 1) {
								setTimeout(function() {
									var error = false;
									for(var j = 1; j <= organizationaddresses; j++) {
										if(request.indexOf("lon" + j) == -1 || request.indexOf("lat" + j) == -1) {
											error = true;
										}
									}
									if(!error) {
										if(action.indexOf("kiezkartei_register_organization") == 0) {
											$.post(kiezkartei_ajax.url, request, onRegistrationResult);
										}
										if(action.indexOf("kiezkartei_edit_organization") == 0) {
											$.post(kiezkartei_ajax.url, request, onEditResult);

										}
									}
								}, 1250);
							} else {
								if(action.indexOf("kiezkartei_register_organization") == 0) {
									$.post(kiezkartei_ajax.url, request, onRegistrationResult);
								}
								if(action.indexOf("kiezkartei_edit_organization") == 0) {
									$.post(kiezkartei_ajax.url, request, onEditResult);
								}
								if(action.indexOf("kiezkartei_add_contact_address") == 0) {
									$.post(kiezkartei_ajax.url, request, onAddResult);
								}
							}
						}
					});
			}

			event.preventDefault();
		});
	});
}

function onRegistrationResult(response) {
	showDialog(response.success ? "Registrierung abgeschlossen" : "Fehler bei der Registrierung", response.data.message,
		response.success ? function() {
			$(location).attr('href', String(getBaseUrl()).replace("register", ""));
		} : undefined);
}
function onEditResult(response) {
	showDialog(response.success ? "Erfolgreich editiert" : "Fehler beim Editieren", response.data.message,
		response.success ? function() {
			location.reload();
		} : undefined);
}
function onAddResult(response) {
	showDialog(response.success ? "Erfolgreich hinzugefügt" : "Fehler beim Hinzufügen", response.data.message,
		response.success ? function() {
			location.reload();
		} : undefined);
}

function onDeleteResult(response) {
	showDialog(response.success ? "Erfolgreich gelöscht!" : "Fehler beim Löschen", response.data.message,
		response.success ? function() {
			location.reload();
		} : undefined);
}

function getCategoriesForRegistrationForm() {
	var data = {
		action : 'kiezkartei_get_categories',
		nonce : kiezkartei_ajax.nonce
	};

	jQuery.post(kiezkartei_ajax.url, data, function(response) {
		if(response.success && response.data.length > 0) {
			var checkboxes = $("#checkboxes");
			var content = "";
			for(var i = 0; i < response.data.length; i++) {

				//var cb = "<label class='pure-checkbox'><input id='checkbox_" + response.data[i] + "' type='checkbox' value='true' name='" + response.data[i] + "1'>" + translate(response.data[i]) + "</label>";
				if (response.data[i][2] === null) {
					content += "<div class='cat'>";
					content += "<fieldset>";
					content += "<div class='toplevelcat'>";
					content += "<label class='pure-checkbox'><input class='parentCheckBox' id='checkbox_" + response.data[i][1] + "' type='checkbox' value='true' name='" + response.data[i][1] + "1'>" + translate(response.data[i][1]) + "</label>";
					content += "</div>";
					content += "<div class='sublevel'>";
					content += "<ul>";
					for (var j = 0; j < response.data.length; j++) {
						
						if (response.data[j][2] == response.data[i][0]) {
							content += "<li><label class='pure-checkbox'><input class='childCheckBox' id='checkbox_" + response.data[j][1] + "' type='checkbox' value='true' name='" + response.data[j][1] + "1'>" + translate(response.data[j][1]) + "</label></li>";
						}
						
					}
					content += "</ul>";
					content += "</div>";
					content += "</fieldset>";
					content += "</div>";
				}
			}
			checkboxes.append(content);
		}
	});
}

function copyFormData() {
	var newaddress = $("#organizationaddress1").eq(0).clone(true);

	var i = 2;
	while($("#organizationaddress" + i).length > 0) {
		i++;
	}

	newaddress.find('input').each(function() {
		if (!(this.type == "checkbox")) {
			this.name = this.name.substring(0, this.name.length - 1) + i;
			this.id = this.name;
		} else {
			this.name = this.name.substring(0, this.name.length - 3) + i + "[]";
			this.id = this.id.substring(0, this.id.length - 1) + i;
		}
	});

	newaddress.find('label').each(function() {
		$(this).attr("for", $(this).attr("for").substring(0, $(this).attr("for").length - 1) + i);
	});

	newaddress.find('textarea').each(function() {
		this.name = this.name.substring(0, this.name.length - 1) + i;
		this.id = this.name;
	});

	newaddress.attr("id", "organizationaddress" + i);

	$('#organizationaddress' + (i - 1)).after(newaddress).after("<br><br>");
	$('#count-organizations').attr("value", i);
}

function getUserdata(id) {
	function createForm(data) {
		$("#organisation-name").val(data.userdata[0].display_name);
		$("#organisation-email").val(data.userdata[0].user_login);
		// clear autofilled passwordfield
		$("#organisation-password").val("");
		registerRegistrationFormButton("kiezkartei_edit_organization");
		if(data.organizations.length > 0) {
			for(var i = 0; i < data.organizations.length; i++) {
				$("#organization-select").append('<option value=' + i + '>' + data.organizations[i].street + '</option>');
			}
			$("#organization-select").css('display', 'inline');
			$("#organization-select").change(function() {
				var selected = $(this).find('option:selected').val();
				if(selected !== "new") {
					fillContactAddress(data.organizations[selected]);
					registerRegistrationFormButton("kiezkartei_edit_organization");
					$("#editOrganization").text("Editieren");
					$("#deleteOrganization").css("display", "inline-block");

				} else {
					$("#deleteOrganization").css("display", "none");
					$('#organizationaddress').find('input:not(input[type=checkbox])').val('');
					$('#organisation-current-need').val('');
					$("#organisation-contact-email").val('');
					var cb = $('#organizationaddress').find('input:checkbox');
					for(var j = 0; j < cb.length; j++) {
						cb[j].checked = false;
					}

					registerRegistrationFormButton("kiezkartei_add_contact_address");
					$("#editOrganization").text("Hinzufügen");
				}
			});
			fillContactAddress(data.organizations[0]);
			$("#organization-select").append('<option value=new>Neuen Standort hinzufügen</option>');
                        console.log(data.organizations.length);
			if(data.organizations.length > 1) {
				$("#deleteOrganization").css("display", "inline-block");
			}
		} else if(data.organizations.length === 0) {
			$("#organizationaddress").show();
			$("#copy").show();
			registerRegistrationFormButton("kiezkartei_add_contact_address");
		}
	}

	function fillContactAddress(organization) {
		$("#organizationaddress").show();
		$("#organisation-contact-name").val(organization.contact_name);
		$("#organisation-contact-email").val(organization.contact_email);
		$("#organisation-contact-phone-number").val(organization.contact_phone_number);
		$("#organisation-contact-street1").val(organization.street);
		$("#organisation-contact-city1").val(organization.city);
		$("#organisation-contact-zipcode1").val(organization.zipcode);
		$("#organisation-contact-url").val(organization.url);
		$("#organisation-current-need").val(organization.current_need);
		$("#organizationid").val(organization.id);
		$("#organisation-profile").val(organization.profile);
		refreshCategories(organization.id);
	}


	var data = {
		action : 'kiezkartei_get_userdata',
		userid : id,
		nonce : kiezkartei_ajax.nonce
	};

	jQuery.post(kiezkartei_ajax.url, data, function(response) {
		if(response.success && response.data !== 0) {
			createForm(response.data);
		}
	});
}

function registerDeleteButton(id) {
	$(document).ready(function() {
		$("#" + id).on('click', function(event) {
			var data = {
				action : "kiezkartei_delete_contact_address",
				orgid : $("#organizationid").val(),
				nonce : kiezkartei_ajax.nonce
			};
			showDialog("Organisation löschen?", "Wollen Sie diese Kontaktadresse wirklich löschen?", function() {
				jQuery.post(kiezkartei_ajax.url, data, onDeleteResult);
			}, true);
			event.preventDefault();
		});
	});
}

function registerMobileButton() {
	jQuery(document).ready(function() {
		$("#mobileoptions").on('click', function(event) {
			$(".mobile ul.pure-menu-children").slideToggle("fast");
			$(".mobile form.home-menu-form.pure-form").slideToggle('fast', function() {
				if($('.mobile form.home-menu-form.pure-form').is(':visible'))
					$('.mobile form.home-menu-form.pure-form').css('display', 'inline-block');
			});
		});
	});
}
