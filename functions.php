<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width)) {
	$content_width = 900;
}

if (function_exists('add_theme_support')) {
	// Add Menu Support
	add_theme_support('menus');

	// Add Thumbnail Theme Support
	add_theme_support('post-thumbnails');
	add_image_size('large', 700, '', true); // Large Thumbnail
	add_image_size('medium', 250, '', true); // Medium Thumbnail
	add_image_size('small', 120, '', true); // Small Thumbnail
	add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

	// Add Support for Custom Backgrounds - Uncomment below if you're going to use
	/*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
	));*/

	// Add Support for Custom Header - Uncomment below if you're going to use
	/*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
	));*/

	// Enables post and comment RSS feed links to head
	add_theme_support('automatic-feed-links');

	// Localisation Support
	load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
		array(
			'theme_location' => 'header-menu',
			'menu' => '',
			'container' => 'div',
			'container_class' => 'menu-{menu slug}-container',
			'container_id' => '',
			'menu_class' => 'menu',
			'menu_id' => '',
			'echo' => true,
			'fallback_cb' => 'wp_page_menu',
			'before' => '',
			'after' => '',
			'link_before' => '',
			'link_after' => '',
			'items_wrap' => '<ul>%3$s</ul>',
			'depth' => 0,
			'walker' => ''
		)
	);
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
	if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

		wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
		wp_enqueue_script('conditionizr'); // Enqueue it!

		wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
		wp_enqueue_script('modernizr'); // Enqueue it!

		wp_register_script('html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
		wp_enqueue_script('html5blankscripts'); // Enqueue it!
	}
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
	if (is_page('pagenamehere')) {
		wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
		wp_enqueue_script('scriptname'); // Enqueue it!
	}
}

// Load HTML5 Blank styles
function html5blank_styles()
{
	wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
	wp_enqueue_style('normalize'); // Enqueue it!

	wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
	wp_enqueue_style('html5blank'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
	register_nav_menus(array( // Using array to specify more menus if needed
		'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
		'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
		'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
	));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
	$args['container'] = false;
	return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
	return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
	return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
	global $post;
	if (is_home()) {
		$key = array_search('blog', $classes);
		if ($key > -1) {
			unset($classes[$key]);
		}
	} elseif (is_page()) {
		$classes[] = sanitize_html_class($post->post_name);
	} elseif (is_singular()) {
		$classes[] = sanitize_html_class($post->post_name);
	}

	return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar')) {
	// Define Sidebar Widget Area 1
	register_sidebar(array(
		'name' => __('Widget Area 1', 'html5blank'),
		'description' => __('Description for this widget-area...', 'html5blank'),
		'id' => 'widget-area-1',
		'before_widget' => '<div id="%1$s" class="%2$s widget-area-1">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));

	// Define Sidebar Widget Area 2
	register_sidebar(array(
		'name' => __('Widget Area 2', 'html5blank'),
		'description' => __('Description for this widget-area...', 'html5blank'),
		'id' => 'widget-area-2',
		'before_widget' => '<div id="%1$s" class="%2$s widget-area-2">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
	global $wp_widget_factory;
	remove_action('wp_head', array(
		$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
		'recent_comments_style'
	));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
	global $wp_query;
	$big = 999999999;
	echo paginate_links(array(
		'base' => str_replace($big, '%#%', get_pagenum_link($big)),
		'format' => '?paged=%#%',
		'current' => max(1, get_query_var('paged')),
		'total' => $wp_query->max_num_pages
	));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
	return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
	return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
	global $post;
	if (function_exists($length_callback)) {
		add_filter('excerpt_length', $length_callback);
	}
	if (function_exists($more_callback)) {
		add_filter('excerpt_more', $more_callback);
	}
	$output = get_the_excerpt();
	$output = apply_filters('wptexturize', $output);
	$output = apply_filters('convert_chars', $output);
	$output = '<p>' . $output . '</p>';
	echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
	global $post;
	return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
	return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
	return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions($html)
{
	$html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
	return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar($avatar_defaults)
{
	$myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
	$avatar_defaults[$myavatar] = "Custom Gravatar";
	return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
	if (!is_admin()) {
		if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
			wp_enqueue_script('comment-reply');
		}
	}
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ('div' == $args['style']) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
	?>
	<!-- heads up: starting < for the html tag (li or div) in the next line: -->
	<<?php echo $tag ?> <?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ('div' != $args['style']) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
<?php endif; ?>
	<div class="comment-author vcard">
		<?php if ($args['avatar_size'] != 0) echo get_avatar($comment, $args['180']); ?>
		<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
	<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br/>
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a
			href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>">
			<?php
			printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'), '  ', '');
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
		<?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ('div' != $args['style']) : ?>
	</div>
<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
	register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
	register_taxonomy_for_object_type('post_tag', 'html5-blank');
	register_post_type('html5-blank', // Register Custom Post Type
		array(
			'labels' => array(
				'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
				'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
				'add_new' => __('Add New', 'html5blank'),
				'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
				'edit' => __('Edit', 'html5blank'),
				'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
				'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
				'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
				'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
				'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
				'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
				'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
			),
			'public' => true,
			'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
			'has_archive' => true,
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail'
			), // Go to Dashboard Custom HTML5 Blank post for supports
			'can_export' => true, // Allows export in Tools > Export
			'taxonomies' => array(
				'post_tag',
				'category'
			) // Add Category and Post Tags support
		));
}

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
	return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
	return '<h2>' . $content . '</h2>';
}

/*------------------------------------*\
   AJAX Database Query Stuff
   http://stackoverflow.com/questions/28022456/how-to-receive-data-from-database-via-jquery-wordpress-js
\*------------------------------------*/

// script style add at frontend
add_action('wp_enqueue_scripts', 'kiezkartei_register_ajax');

function kiezkartei_register_ajax()
{
	wp_deregister_script('jquery');
	wp_register_script('main', get_template_directory_uri() . '/js/main.js', array());
	wp_enqueue_script('main'); // Enqueue it!
	// localize the script
	wp_localize_script('main', 'kiezkartei_ajax', array('url' => admin_url('admin-ajax.php'), 'nonce' => wp_create_nonce("ajax_call_nonce")));
}

// action for execute ajax from frontend for user which are not logged in
add_action('wp_ajax_nopriv_kiezkartei_get_mapdata', 'kiezkartei_get_mapdata');
// make the function available for logged in users
add_action('wp_ajax_kiezkartei_get_mapdata', 'kiezkartei_get_mapdata');

function kiezkartei_get_mapdata()
{
	$nonce = check_ajax_referer('ajax_call_nonce', 'nonce');

	if ($nonce == true) {
		global $wpdb;

		try {
			$organizations = $wpdb->get_results($wpdb->prepare("SELECT * FROM organizations WHERE register_email != 'user@example.com' ORDER BY name ASC"));
		} catch (Exception $e) {
			$organizations = array();
		}
		try {
			$categories = $wpdb->get_results($wpdb->prepare("SELECT * FROM categories ORDER BY name ASC"));
		} catch (Exception $e) {
			$categories = array();
		}
		try {
			$org_cat_mapping = $wpdb->get_results($wpdb->prepare("SELECT * FROM org_cat_mapping"));
		} catch (Exception $e) {
			$org_cat_mapping = array();
		}

		//remove debbuging info
		ob_clean();
		$obj = (object)array('organizations' => $organizations, 'categories' => $categories, 'org_cat_mapping' => $org_cat_mapping);
		ob_clean();
		wp_send_json_success($obj);
	} else {
		wp_send_json_error();
	}
}

// action for execute ajax from frontend for user which are not logged in
add_action('wp_ajax_nopriv_kiezkartei_register_organization', 'kiezkartei_register_organization');
// make the function available for logged in users
add_action('wp_ajax_kiezkartei_register_organization', 'kiezkartei_register_organization');

function kiezkartei_send_ajax_error($msg)
{
	wp_send_json_error(array('message' => $msg));
}

function kiezkartei_get_post_arg($name)
{
	if (isset($_POST[$name]))
		return $_POST[$name];

	return "";
}

function kiezkartei_register_user($organisation_name, $email, $password)
{
	$userdata = array(
		'user_login' => $email,
		'user_email' => $email,
		'user_pass' => $password,
		'display_name' => $organisation_name
	);

	$id = wp_insert_user($userdata);

	if (is_wp_error($id))
		return $id->get_error_message();

	// return userid
	return $id;
}

function kiezkartei_register_organization()
{
	$nonce = check_ajax_referer('ajax_call_nonce', 'nonce');

	if (!$nonce) {
		kiezkartei_send_ajax_error('Unbekannter Fehler aufgetreten, bitte lade die Seite erneut!');
		return;
	}

	$organisation_name = kiezkartei_get_post_arg("organisation-name", "");

	if (empty($organisation_name)) {
		kiezkartei_send_ajax_error("Bitte geben Sie den Namen der Organisation an!");
		return;
	}

	$organisation_email = kiezkartei_get_post_arg("organisation-email", "");

	if (empty($organisation_email) || ($email_exist = email_exists($organisation_email))) {
		if ($email_exist)
			kiezkartei_send_ajax_error('Ein Account mit der Adresse "' . $organisation_email . '" existiert bereits!');
		else
			kiezkartei_send_ajax_error("Bitte geben Sie eine E-Mail Adresse für die Registrierung an!");

		return;
	}

	$organisation_password = kiezkartei_get_post_arg("organisation-password");

	if (empty($organisation_password)) {
		kiezkartei_send_ajax_error("Bitte geben Sie ein Passwort an!");
		return;
	}

	$result = kiezkartei_register_user($organisation_name, $organisation_email, $organisation_password);

	$organisation_profile = kiezkartei_get_post_arg("organisation-profile");

	if (!is_numeric($result)) {
		// it's the error message
		kiezkartei_send_ajax_error($result);
		return;
	}
	// its the userid
	$userid = $result;

	$organisationaddresses = kiezkartei_get_post_arg("count");

	if (!is_numeric($organisationaddresses)) {
		kiezkartei_send_ajax_error($organisationaddresses);
		return;
	}

	// get all category names
	global $wpdb;
	//$cats = $wpdb->get_results($wpdb->prepare("SELECT * FROM categories"));

	for ($i = 1; $i <= $organisationaddresses; $i++) {
		$organisation_contact_name = kiezkartei_get_post_arg("organisation-contact-name" . $i);

		if (empty($organisation_contact_name)) {
			kiezkartei_send_ajax_error("Bitte geben Sie einen Kontaktnamen an!");
			return;
		}

		$organisation_contact_email = kiezkartei_get_post_arg("organisation-contact-email" . $i);

		if (empty($organisation_contact_email)) {
			kiezkartei_send_ajax_error("Bitte geben Sie eine Kontakt E-Mail-Adresse an!");
			return;
		}

		$organisation_contact_phone_number = kiezkartei_get_post_arg("organisation-contact-phone-number" . $i);

		if (empty($organisation_contact_phone_number)) {
			kiezkartei_send_ajax_error("Bitte geben Sie eine Telefonnummer an!");
			return;
		}

		$organisation_contact_street = kiezkartei_get_post_arg("organisation-contact-street" . $i);

		if (empty($organisation_contact_street)) {
			kiezkartei_send_ajax_error("Bitte geben Sie Straße an!");
			return;
		}

		$organisation_contact_city = kiezkartei_get_post_arg("organisation-contact-city" . $i);

		if (empty($organisation_contact_city)) {
			kiezkartei_send_ajax_error("Bitte geben Sie eine Stadt an!");
			return;
		}

		$organisation_contact_zipcode = kiezkartei_get_post_arg("organisation-contact-zipcode" . $i);

		if (empty($organisation_contact_zipcode)) {
			kiezkartei_send_ajax_error("Bitte geben Sie eine Postleitzahl an!");
			return;
		}

		$organisation_contact_url = kiezkartei_get_post_arg("organisation-contact-url" . $i);


		$organisation_contact_lon = kiezkartei_get_post_arg("lon" . $i);
		$organisation_contact_lat = kiezkartei_get_post_arg("lat" . $i);

		if (empty($organisation_contact_lon) || empty($organisation_contact_lat)) {
			kiezkartei_send_ajax_error("Fehler bei der Ermittlung der Adresse! Bitte versuchen sie es erneut oder wenden Sie sich an den Administrator.");
			return;
		}

		$organisation_text = kiezkartei_get_post_arg("organisation-current-need" . $i);
		$result = $wpdb->insert(
			"organizations",
			array("name" => $organisation_name, "register_email" => "user@example.com", "contact_email" => $organisation_contact_email,
				"contact_phone_number" => $organisation_contact_phone_number, "contact_name" => $organisation_contact_name,
				"street" => $organisation_contact_street, "city" => $organisation_contact_city, "zipcode" => $organisation_contact_zipcode,
				"url" => $organisation_contact_url, "longitude" => $organisation_contact_lon, "latitude" => $organisation_contact_lat, "userid" => $userid, "current_need" => $organisation_text, "profile" => $organisation_profile),
                        array("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%f", "%f", "%0.0f", "%s", "%s"));
		$orgid = $wpdb->insert_id;
		
		if (!$result) {
			kiezkartei_send_ajax_error("Es gab einen Datenbankfehler, bitte informieren Sie den Administrator!");
			return;
		}


		

		if (isset($_POST["category" . $i])) {
			foreach ($_POST["category" . $i] as $category) {
				
				kiezkartei_add_organisationcategory($orgid, $category);
			}
		}

		if (isset($_POST["category_" . $i])) {
			foreach ($_POST["category_" . $i] as $subcategory) {
				
				kiezkartei_add_organisationcategory($orgid, $subcategory);
			}
		}

	}

	ob_clean();
	wp_send_json_success(array("message" => "Registrierung erfolgreich abgeschlossen! 
                        Sobald ihr Account von einem Administrator freigeschaltet wurde, werden sie per E-Mail benachrichtigt!"));
}


function kiezkartei_add_organisationcategory($id, $catid)
{
	global $wpdb;
	$result = $wpdb->insert("org_cat_mapping", array("organizationid" => $id, "categoryid" => $catid), array("%d", "%d"));

	return $result;
}


// Kiezkartei functions
function kiezkartei_add_query_vars_filter($vars)
{
	$vars[] = "post";
	return $vars;
}

add_filter('query_vars', 'kiezkartei_add_query_vars_filter');

// user approval
add_action('new_user_approve_user_approved', 'kiezkartei_approve_user');

function kiezkartei_approve_user($user)
{
	global $wpdb;
	$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_users WHERE id = '" . $user->id . "' "));
	foreach ($results as $result) {
		$result = $wpdb->update("organizations", array("register_email" => $result->user_login), array("userid" => $user->id), array("%s"), array("%f"));
	}
}


// function which returns all coloumn names from the table categories
// will be used to dynamically create the checkboxes on the register form

// action for execute ajax from frontend for user which are not logged in
add_action('wp_ajax_nopriv_kiezkartei_get_categories', 'kiezkartei_get_categories');
// make the function available for logged in users
add_action('wp_ajax_kiezkartei_get_categories', 'kiezkartei_get_categories');

function kiezkartei_get_categories()
{
	$nonce = check_ajax_referer('ajax_call_nonce', 'nonce');
	global $wpdb;
	$categories = array();

	$cats = $wpdb->get_results($wpdb->prepare("SELECT id, name, parent FROM categories"));
	foreach ($cats as $cat) {
		array_push($categories, array($cat->id, $cat->name, $cat->parent));
	}


	ob_clean();
	wp_send_json_success($categories);
}


// remove organizations table if user is deleted
add_action('delete_user', 'delete_organization');
function delete_organization($user_id)
{
	global $wpdb;
	$wpdb->delete("organizations", array("userid" => $user_id), array("%d"));
}


// make the function available for logged in users
add_action('wp_ajax_kiezkartei_get_userdata', 'kiezkartei_get_userdata');

function kiezkartei_get_userdata()
{
	$nonce = check_ajax_referer('ajax_call_nonce', 'nonce');
	$userid = $_POST["userid"];
	if ($nonce == true) {
		global $wpdb;

		try {
			$organizations = $wpdb->get_results($wpdb->prepare("SELECT * FROM organizations WHERE userid =" . $userid));
		} catch (Exception $e) {
			$organizations = array();
		}
		try {
			$userdata = $wpdb->get_results($wpdb->prepare("SELECT user_login, display_name FROM wp_users WHERE id =" . $userid));
		} catch (Exception $e) {
			$userdata = array();
		}

		// !!!!!!!!!!!!!!!!!!!!!!!!!!!@BASTI!!!!!!!!!!!!!!!!!!!!
		// mach das hier so wie in kiezkartei_get_mapdata()
		// ich würde den categorie namen erst client seitig mit zur category id mappen
		// was die hier machen ist SCHROTT
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!@BASTI!!!!!!!!!!!!!!!!!!!!
		try {
			foreach ($organizations as $organization) {
				// $strQuery  = "SELECT * FROM categories WHERE %s=categories.organizationid";
				// $organization->categories = $wpdb->get_results($wpdb->prepare($strQuery, $organization->id));
				$categories = $wpdb->get_results($wpdb->prepare("SELECT * FROM categories"));
				$orgcats = $wpdb->get_results($wpdb->prepare("SELECT * FROM org_cat_mapping WHERE '" . $organization->id . "'=organizationid"));

				$organization->categories = array();
				foreach ($categories as $category) {
					$set = 0;
					foreach ($orgcats as $orgcat) {
						if ($orgcat->categoryid == $category->id) {
							$set = 1;
							break;
						}
					}
					$mycategory = new Category();
					$mycategory->name = $category->name;
					$mycategory->set = $set;

					array_push($organization->categories, $mycategory);
				}
			}
		} catch (Exception $e) {
			ob_clean();
			wp_send_json_success($e);
		}

		//remove debbuging info
		ob_clean();
		$obj = (object)array('organizations' => $organizations, 'userdata' => $userdata);

		wp_send_json_success($obj);
	} else {
		wp_send_json_error();
	}
}


// make the function available for logged in users
add_action('wp_ajax_kiezkartei_edit_organization', 'kiezkartei_edit_organization');
function kiezkartei_edit_organization()
{
	$nonce = check_ajax_referer('ajax_call_nonce', 'nonce');

	if (!$nonce) {
		kiezkartei_send_ajax_error('Unbekannter Fehler aufgetreten, bitte lade die Seite erneut!');
		return;
	}

	$organizationid = kiezkartei_get_post_arg("orgid");
	$userid = kiezkartei_get_post_arg("userid");

	$organisation_name = kiezkartei_get_post_arg("organisation-name");

	if (empty($organisation_name)) {
		kiezkartei_send_ajax_error("Bitte geben Sie den Namen der Organisation an!");
		return;
	}

	$organisation_email = kiezkartei_get_post_arg("organisation-email");

	$email_exist = email_exists($organisation_email);

	if (is_numeric($email_exist) && $email_exist != $userid) {
		kiezkartei_send_ajax_error('Ein Account mit der Adresse "' . $organisation_email . '" existiert bereits!');
		return;
	}
	if (empty($organisation_email)) {
		kiezkartei_send_ajax_error("Bitte geben Sie eine E-Mail Adresse für die Registrierung an!");
		return;
	}

	$organisation_profile = kiezkartei_get_post_arg("organisation-profile");

	$result = kiezkartei_edit_user($userid, $organisation_name, $organisation_email);

	if (false === $result) {
		// it's the error message
		kiezkartei_send_ajax_error($result);
		return;
	}

	// get all category names
	global $wpdb;
	ob_clean();

	$organisation_contact_name = kiezkartei_get_post_arg("organisation-contact-name");

	if (empty($organisation_contact_name)) {
		kiezkartei_send_ajax_error("Bitte geben Sie einen Kontaktnamen an!");
		return;
	}

	$organisation_contact_email = kiezkartei_get_post_arg("organisation-contact-email");

	if (empty($organisation_contact_email)) {
		kiezkartei_send_ajax_error("Bitte geben Sie eine Kontakt E-Mail-Adresse an!");
		return;
	}

	$organisation_contact_phone_number = kiezkartei_get_post_arg("organisation-contact-phone-number");

	if (empty($organisation_contact_phone_number)) {
		kiezkartei_send_ajax_error("Bitte geben Sie eine Telefonnummer an!");
		return;
	}
	$organisation_contact_street = kiezkartei_get_post_arg("organisation-contact-street1");

	if (empty($organisation_contact_street)) {
		kiezkartei_send_ajax_error("Bitte geben Sie Straße an!");
		return;
	}

	$organisation_contact_city = kiezkartei_get_post_arg("organisation-contact-city1");

	if (empty($organisation_contact_city)) {
		kiezkartei_send_ajax_error("Bitte geben Sie eine Stadt an!");
		return;
	}

	$organisation_contact_zipcode = kiezkartei_get_post_arg("organisation-contact-zipcode1");

	if (empty($organisation_contact_zipcode)) {
		kiezkartei_send_ajax_error("Bitte geben Sie eine Postleitzahl an!");
		return;
	}

	$organisation_contact_url = kiezkartei_get_post_arg("organisation-contact-url");

	$organisation_contact_lon = kiezkartei_get_post_arg("lon1");
	$organisation_contact_lat = kiezkartei_get_post_arg("lat1");

	if (empty($organisation_contact_lon) || empty($organisation_contact_lat)) {
		kiezkartei_send_ajax_error("Fehler bei der Ermittlung der Adresse! Bitte versuchen sie es erneut oder wenden Sie sich an den Administrator.");
		return;
	}

	$organisation_text = kiezkartei_get_post_arg("organisation-current-need");
	$organisation_profile = kiezkartei_get_post_arg("organisation-profile");

	$result = $wpdb->update(
		"organizations",
		array(
			"contact_phone_number" => $organisation_contact_phone_number, "contact_name" => $organisation_contact_name, "contact_email" => $organisation_contact_email,
			"street" => $organisation_contact_street, "city" => $organisation_contact_city, "zipcode" => $organisation_contact_zipcode,
			"url" => $organisation_contact_url, "longitude" => $organisation_contact_lon, "latitude" => $organisation_contact_lat, "userid" => $userid, "current_need" => $organisation_text),
		array("id" => $organizationid),
		array("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%f", "%f", "%0.0f", "%s"),
		array('%d'));

	if (false === $result) {
		kiezkartei_send_ajax_error("Fehler beim Editieren.");
		return;
	}

	$result = $wpdb->update(
		"organizations",
		array("name" => $organisation_name, "register_email" => $organisation_email,
			 "profile" => $organisation_profile),
		array("userid" => $userid),
		array("%s", "%s", "%s"),
		array('%d'));

	if (false === $result) {
		kiezkartei_send_ajax_error("Fehler beim Editieren.");
		return;
	}

	$wpdb->delete("org_cat_mapping", array("organizationid" => $organizationid), array("%d"));
	
	
	if (isset($_POST["category"])) {
		foreach ($_POST["category"] as $category) {
			kiezkartei_add_organisationcategory($organizationid, $category);
		}
	}

	if (isset($_POST["category_"])) {
		foreach ($_POST["category_"] as $subcategory) {
			kiezkartei_add_organisationcategory($organizationid, $subcategory);
		}
	}

	ob_clean();
	wp_send_json_success(array("message" => "Edititeren erfolgreich abgeschlossen!"));
}

function kiezkartei_edit_user($userid, $organisation_name, $email)
{
	global $wpdb;

	$userdata = array(
		'user_login' => $email,
		'user_email' => $email,
		'display_name' => $organisation_name
	);
	$types = array(
		'%s',
		'%s',
		'%s'
	);


	$result = $wpdb->update(
		'wp_users',
		$userdata,
		array('ID' => $userid),
		$types,
		array('%d')
	);

	return $result;

}

// make the function available for logged in users
add_action('wp_ajax_kiezkartei_delete_contact_address', 'kiezkartei_delete_contact_address');
function kiezkartei_delete_contact_address()
{
	$nonce = check_ajax_referer('ajax_call_nonce', 'nonce');

	if (!$nonce) {
		kiezkartei_send_ajax_error('Unbekannter Fehler aufgetreten, bitte lade die Seite erneut!');
		return;
	}
	$organizationid = kiezkartei_get_post_arg("orgid");
	global $wpdb;

	$wpdb->delete("organizations", array("id" => $organizationid), array("%d"));
	ob_clean();
	wp_send_json_success(array("message" => "Kontaktadresse erfolgreich entfernt!"));
}

// make the function available for logged in users
add_action('wp_ajax_kiezkartei_add_contact_address', 'kiezkartei_add_contact_address');
function kiezkartei_add_contact_address()
{

	$nonce = check_ajax_referer('ajax_call_nonce', 'nonce');
	if (!$nonce) {
		kiezkartei_send_ajax_error('Unbekannter Fehler aufgetreten, bitte lade die Seite erneut!');
		return;
	}
	$userid = kiezkartei_get_post_arg("userid");


	// also edit the organisation name if the user wants to add a location and change some of the general things
	$organisation_name = kiezkartei_get_post_arg("organisation-name");
	if (empty($organisation_name)) {
		kiezkartei_send_ajax_error("Bitte geben Sie den Namen der Organisation an!");
		return;
	}
	$organisation_email = kiezkartei_get_post_arg("organisation-email", "");
	$email_exist = email_exists($organisation_email);
	if (is_numeric($email_exist) && $email_exist != $userid) {
		kiezkartei_send_ajax_error('Ein Account mit der Adresse "' . $organisation_email . '" existiert bereits!');
		return;
	}
	if (empty($organisation_email)) {
		kiezkartei_send_ajax_error("Bitte geben Sie eine E-Mail Adresse für die Registrierung an!");
		return;
	}
	$result = kiezkartei_edit_user($userid, $organisation_name, $organisation_email);
	if (false === $result) {
		// it's the error message
		kiezkartei_send_ajax_error($result);
		return;
	}
	////////////////////////////////////////////////////////////////////////////////
	// get all category names
	global $wpdb;
	ob_clean();


	$organisation_contact_name = kiezkartei_get_post_arg("organisation-contact-name");

	if (empty($organisation_contact_name)) {
		kiezkartei_send_ajax_error("Bitte geben Sie einen Kontaktnamen an!");
		return;
	}

	$organisation_contact_email = kiezkartei_get_post_arg("organisation-contact-email");

	if (empty($organisation_contact_email)) {
		kiezkartei_send_ajax_error("Bitte geben Sie eine Kontakt E-Mail-Adresse an!");
		return;
	}

	$organisation_contact_phone_number = kiezkartei_get_post_arg("organisation-contact-phone-number");

	if (empty($organisation_contact_phone_number)) {
		kiezkartei_send_ajax_error("Bitte geben Sie eine Telefonnummer an!");
		return;
	}

	$organisation_contact_street = kiezkartei_get_post_arg("organisation-contact-street1");

	if (empty($organisation_contact_street)) {
		kiezkartei_send_ajax_error("Bitte geben Sie Straße an!");
		return;
	}

	$organisation_contact_city = kiezkartei_get_post_arg("organisation-contact-city1");

	if (empty($organisation_contact_city)) {
		kiezkartei_send_ajax_error("Bitte geben Sie eine Stadt an!");
		return;
	}

	$organisation_contact_zipcode = kiezkartei_get_post_arg("organisation-contact-zipcode1");

	if (empty($organisation_contact_zipcode)) {
		kiezkartei_send_ajax_error("Bitte geben Sie eine Postleitzahl an!");
		return;
	}

	$organisation_contact_url = kiezkartei_get_post_arg("organisation-contact-url");

	$organisation_contact_lon = kiezkartei_get_post_arg("lon1");
	$organisation_contact_lat = kiezkartei_get_post_arg("lat1");

	if (empty($organisation_contact_lon) || empty($organisation_contact_lat)) {
		kiezkartei_send_ajax_error("Fehler bei der Ermittlung der Adresse! Bitte versuchen sie es erneut oder wenden Sie sich an den Administrator.");
		return;
	}

	$organisation_text = kiezkartei_get_post_arg("organisation-current-need");

	$organisation_profile = kiezkartei_get_post_arg("organisation-profile");

	global $wpdb;

	$result = $wpdb->insert(
		"organizations",
		array("name" => $organisation_name, "register_email" => $organisation_email, "contact_email" => $organisation_contact_email,
			"contact_phone_number" => $organisation_contact_phone_number, "contact_name" => $organisation_contact_name,
			"street" => $organisation_contact_street, "city" => $organisation_contact_city, "zipcode" => $organisation_contact_zipcode,
			"url" => $organisation_contact_url, "longitude" => $organisation_contact_lon, "latitude" => $organisation_contact_lat, "userid" => $userid, "current_need" => $organisation_text, "profile" => $organisation_profile),
		array("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%f", "%f", "%0.0f", "%s", "%s"));
	$orgid = $wpdb->insert_id;

	if (!$result) {
		kiezkartei_send_ajax_error("Es gab einen Datenbankfehler, bitte informieren Sie den Administrator!");
		return;
	}

	if (isset($_POST["category"])) {
		foreach ($_POST["category"] as $category) {
			kiezkartei_add_organisationcategory($orgid, $category);
		}
	}

	if (isset($_POST["category_"])) {
		foreach ($_POST["category_"] as $subcategory) {
			kiezkartei_add_organisationcategory($orgid, $subcategory);
		}
	}

	ob_clean();
	wp_send_json_success(array("message" => "Kontaktadresse erfolgreich hinzugefügt!"));
}

class Category
{
	public $name;
	public $set;
}

?>
