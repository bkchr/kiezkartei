-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 02, 2015 at 08:24 
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE IF NOT EXISTS `organizations` (
  `name` varchar(64) NOT NULL,
  `register_email` varchar(100) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `contact_phone_number` varchar(64) NOT NULL,
  `contact_name` varchar(64) NOT NULL,
  `street` varchar(64) NOT NULL,
  `city` varchar(32) NOT NULL,
  `zipcode` varchar(5) NOT NULL,
  `url` varchar(100) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
`id` int(11) NOT NULL,
  `text` varchar(200) NOT NULL,
  `userid` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`name`, `register_email`, `contact_email`, `contact_phone_number`, `contact_name`, `street`, `city`, `zipcode`, `url`, `longitude`, `latitude`, `id`, `text`, `userid`) VALUES
('Arbeiter Samariter Bund', 'user@example.com', 'user@example.com', '0221/4 76 05-376', 'Test', 'Sülzburgstraße 140', 'Köln', '50937', 'https://www.asb.de/de/unsere-angebote/weitere-soziale-dienste/obdachlosenhilfe', 50.919276, 6.92158, 1, '', 0),
('Zentrum für Gesundheit & Kultur', 'kultur@asdf.de', 'g15-buero@obdach-hkp.info', '49 30 695366-14', 'Test', 'Gitschiner Straße 15', 'Berlin', '10969', 'http://www.gitschiner15.de', 13.400609, 52.49843, 2, '', 0),
('A-Z Hilfen Berlin gGmbH', 'user@example.com', 'wohnen@a-z-hilfen.de', '030/843 134 95', 'Test', 'Braunschweiger Str. 28', 'Berlin', '12055', 'www.a-z-hilfen.de', 13.450809, 52.472127, 3, '', 0),
('AWO Landesverband Berlin e.V.', 'user@example.com', 'birgit.muenchow@awoberlin.de', '030 25 389 308', 'Test', 'Blücherstraße 62', 'Berlin', '10961', 'https://www.awoberlin.de/', 13.399751, 52.494519, 4, '', 0),
('Test4', 'test@tesaeweart.de', 'test3@ataset.de', '24535', 'test4', 'Zossener Damm 5C', 'Blankenfelde', '15827', 'awef', 13.400856, 52.330445, 9, '', 0),
('ich', 'lolo@rolocaust.de', 'test3@ataset.de', '24535', 'ich', 'Zossener Damm 5C', 'Blankenfelde', '15827', 'awef', 13.400856, 52.330445, 10, '', 0),
('Nichtfreigeschaltet', 'schaltemichfrei@gmx.xn--nasdflnas-27a', 'stevenboywinter@yahoo.de', '21345', 'Stegven', 'Libauer Straße 19', 'Berlin', '10245', '1988', 13.45448, 52.509262, 11, '', 0),
('ganzneu', '', 'hien@ketchup.daew', '24', 'Heinz', 'Boxhagener Straße 50', 'Berlin', '10245', 'döner.de', 13.46655, 52.50822, 12, '', 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;