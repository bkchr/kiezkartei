<?php
/*
Template Name: Organization Login template
*/
?>

    <?php get_header(); ?>

        <script>
            adjustTopMargin(".content-page");
        </script>
        <?php if(!is_user_logged_in()) { ?>
            <div class="content-page" role="main">
                <div class="orglogin">
                    <h1>Login für Organisationen</h1>
                    <form action="<?php echo wp_login_url(site_url()); ?>" method="post">
                        <input id="user_login" type="text" placeholder="E-Mail" name="log" required>
                        <input id="user_pass" type="password" placeholder="Passwort" name="pwd" required>
                        <button id="wp-submit" type="submit" class="pure-button pure-button-primary" name="wp-submit">Einloggen</button>
                    </form>
                </div>
                <?php }?>
                    <?php
		the_post();
		$page_for_posts_id = get_option('page_for_posts');
		setup_postdata(get_page($page_for_posts_id));
		?>
                        <div id="post-<?php the_ID(); ?>" class="page">
                            <div id="editable-page-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <?php
		rewind_posts();
	?>
            </div>

            <?php get_footer(); ?>